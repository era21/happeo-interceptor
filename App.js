import React from 'react';
import { Strings } from './src/constants/Strings';
import InterceptorCard from './src/components/InterceptorCard';
import runTest from './src/test/Test';

function App() {
  return (
    <InterceptorCard
      buttonText={Strings.BUTTON_TEXT }
      description={Strings.DESCRIPTION }
      buttonPressed={()=>{runTest()} }
    />
  );
}
export default App;
