import client from "../services/ApiClient";
import { ApiEndpoint } from "../constants/ApiConstants";

 export default function runTest() {
    const apiClient = client();
    apiClient.get(ApiEndpoint.BATCH_URL, { params: { ids: ["fileid1", "fileid2"] } })
        .then(res => {
            console.log(res);
        })
        .catch(error => {
            console.log('error', error);
        });

    apiClient.get(ApiEndpoint.BATCH_URL, { params: { ids: ["fileid2"] } })
        .then(res => {
            console.log(res);
        })
        .catch(error => {
            console.log('error', error);
        });

    apiClient.get(ApiEndpoint.BATCH_URL, { params: { ids: ["fileid3"] } })
        .then(res => {
            console.log(res);
        })
        .catch(error => {
            console.log('error', error);
        });

   
};



