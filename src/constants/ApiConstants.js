export const ApiEndpoint = {
    HOST_URL : 'https://europe-west1-quickstart-1573558070219.cloudfunctions.net',
    BASE_URL : 'https://europe-west1-quickstart-1573558070219.cloudfunctions.net',
    BATCH_URL : '/file-batch-api',
};
export const ApiMethod = {
    GET : 'get',
    ERROR_CODE:404
};
