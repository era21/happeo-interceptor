
import axios from "axios";
import batchInterceptor from "./Interceptor";
import { ApiEndpoint } from '../constants/ApiConstants';

const client = () => {
    const config = {
        host: ApiEndpoint.HOST_URL,
        baseURL: ApiEndpoint.BASE_URL,
        headers: {}
    };
    const instance = axios.create(config);
    batchInterceptor(instance);
    return instance;
}
export default client;

