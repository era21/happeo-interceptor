import httpAdapter from "axios/lib/adapters/http";
import createError from "axios/lib/core/createError";
import { ApiEndpoint,ApiMethod } from "../constants/ApiConstants";
import { Strings } from "../constants/Strings";

const batchingPeriod = 300;//Get request batch interval in ms
let batchedRequests = [];
let combinedReqPromise;

{/*
  Returns the merged config for the batched API,
  removed duplicated Ids'
*/}
const getMergedConfig = (config) => {

  let paramIds = batchedRequests.map((req)=> req.params.ids);
  let withoutDuplicates =[...new Set([].concat(...paramIds))]; 

  if (withoutDuplicates && withoutDuplicates.length > 0) 
    return { ...config, params: { ...config.params, ids: withoutDuplicates } };
  else
   return config;
};

{/*
  Updates the original request responses by mapping with 
  batched api response with original request config
*/}
const updateResponse = (config) => {
  return (res) => {
    let paramIds = config?.params?.ids;
    const data = res?.data?JSON.parse(res.data):[];
    const items = data?.items.filter((item) => paramIds.includes(item.id));
    
    if (items && items.length > 0) {
      return Promise.resolve({ ...res, data: { items } });
    }else{
      return Promise.reject(createError(Strings.FILE_NOT_FOUND, config, ApiMethod.ERROR_CODE));
    }

    
  };
};

{/*
  Gets the config for batched API request,
   Returns a promise with the batched API response
   and clears the saved batched requests
*/}
 const batchAdapter = (config) => {
    batchedRequests.push(config);
    if(!combinedReqPromise){
      combinedReqPromise = new Promise((resolve, reject) => {
        setTimeout(() => {
           httpAdapter(getMergedConfig(config))
            .then(resolve)
            .catch(reject)
            .finally(() => (batchedRequests = []));
        }, batchingPeriod);
      });
    }
    return combinedReqPromise;
 
};

function batchInterceptor(instance) {
  instance.interceptors.request.use(
    (request) => {
        //intercepts only file-batch-api get requests
        if(request?.url.includes(ApiEndpoint.BATCH_URL) && request?.method === ApiMethod.GET){
          request.adapter = (config) =>
          batchAdapter(config).then(updateResponse(config));
      }
     
        return request;
    },
    (error) => Promise.reject(error)
  );
}
export default batchInterceptor;

