import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';

const InterceptorCard = ({ buttonText, description, buttonPressed}) => {

  return (
    <View style={styles.container}>
      <Text style={styles.text}>{description}</Text>
      <TouchableOpacity
        onPress={buttonPressed}
        style={styles.button}
      >
        <Text style={styles.btnText}>{buttonText}</Text>
      </TouchableOpacity>

    </View>
  );

};
InterceptorCard.propTypes = {
  buttonText: PropTypes.string,
  description: PropTypes.string,
  buttonPressed: PropTypes.func,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  text: {
    fontSize: 20,
    fontWeight: "600",
    marginTop: 20,
    padding: 20
  },
  button: {
    height: 50,
    width: '80%',
    backgroundColor: '#69D5B9',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8
  },
  btnText: {
    fontWeight: '400',
    color: '#ffffff',
    padding: 8,
    fontSize: 20,
    textAlign: 'center'
  },
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default InterceptorCard;